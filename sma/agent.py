from pygame.math import Vector2
from body import Body
from item import Statut


class Agent:
    def __init__(self, statut):
        self.body = Body()
        self.debug = False
        self.alive = True
        self.statut = Statut(statut)

    def update(self, agents):
        if self.alive:
            self.body.update(agents)
            self.body.move()

    # def move(self):
    #     self.body.move()

    def edge(self, fenetre):
        if self.alive:
            self.body.edge(fenetre)

    def infect(self, agents):
        for agent in agents:
            if agent.body.pos.distance_to(self.body.pos) < self.body.size*2:
                if agent.statut is Statut.Infecte and self.statut is not Statut.Gueri: 
                    self.statut = agent.statut
