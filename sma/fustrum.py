from item import Statut


class Fustrum:
    def __init__(self):
        self.debug = False
        self.perception = 100

    def filtre(self, position, agents):       
        infectesInVision = []
        cibleSain = None
        distCibleSains = 1000
        
        for agent in agents:
            if agent.body.pos.distance_to(position) < self.perception and agent.alive:
                if agent.statut is Statut.Infecte:
                    infectesInVision.append(agent)

                if agent.body.pos.distance_to(position) < distCibleSains :
                    cibleSain = agent
                    distCibleSains = agent.body.pos.distance_to(position)

        return (cibleSain, infectesInVision) 
