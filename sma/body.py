import random
from pygame.math import Vector2
from fustrum import Fustrum
from item import Statut


class Body:
    def __init__(self):
        self.debug = False
        self.pos = Vector2(random.randint(0,800),random.randint(0,800))
        self.vel = Vector2(random.uniform(-5,5),random.uniform(-5,5))
        self.acc = Vector2()
        self.maxAcc = 2
        self.maxSpeed = 5
        self.size = 10
        self.fustrum = Fustrum()

    def attraction(self, cible):
        if cible is not None:
            return cible.body.pos - self.pos

        return Vector2()

    def repultion(self, voisins):
        rep = Vector2()
        for v in voisins:
            rep = rep + self.pos - v.body.pos

        if len(voisins) >= 1:
            return rep / len(voisins)

        return rep

    def update(self, agents):
        (cibleSain, infectesInVision) = self.fustrum.filtre(self.pos, agents)

        acc = Vector2()
        if cibleSain is not None:
            acc = acc + self.attraction(cibleSain)

        if infectesInVision is not None:
            acc = acc + self.repultion(infectesInVision)

        self.acc = acc

        # acceleration is not changed, so explore randomly 
        if acc is Vector2():
            self.acc = Vector2(random.uniform(-self.size, self.size), random.uniform(-self.size, self.size))
    
    def move(self):
        if self.acc.length() > self.maxAcc:
            self.acc.scale_to_length(self.maxAcc)

        self.vel = self.vel + self.acc

        if self.vel.length() > self.maxSpeed:
            self.vel.scale_to_length(self.maxSpeed)

        self.pos = self.pos + self.vel
        self.acc = Vector2(0,0)

    def edge(self, fenetre):
        if self.pos.y < 0:
            self.pos.y = fenetre[1]

        if self.pos.y > fenetre[1]:
            self.pos.y = 0

        if self.pos.x < 0:
            self.pos.x = fenetre[0]

        if self.pos.x > fenetre[0]:
            self.pos.x = 0

    # def collideAgent(self, agents):
    #     for agent in agents:
    #         if agent.body.pos.distance_to(self.pos) < agent.body.size+self.size :
    #             self.vel = -self.vel 

