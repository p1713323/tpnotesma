import random
import core
import pygame
from pygame.math import Vector2

from agent import Agent
from item import Statut


def flipCoin(p):
    r = random.random()
    return r < p

def show():
    for i in range(0, core.memory("nbAgents")):
        if core.memory("agents")[i].alive:
            agent = core.memory("agents")[i]
            # core.Draw.circle((0, 0, 255), agent.body.pos, agent.body.fustrum.perception)
            if agent.statut is Statut.Sains:
                core.Draw.circle((255, 255, 255), agent.body.pos, agent.body.size)
            elif agent.statut is Statut.Infecte:
                core.Draw.circle((255, 0, 0), agent.body.pos, agent.body.size)
            elif agent.statut is Statut.Gueri:
                core.Draw.circle((0, 255, 0), agent.body.pos, agent.body.size)

# def randAddInfectes():
#     if flipCoin(0.7):
#         rand = random.randint(0, 2)
#         if rand == 0:
#             core.memory("nbAgents", core.memory("nbAgents")+1)
#             core.memory("agents").append(Agent())
#             print("[RANDOM] Agents are now : ", core.memory("nbAgents"))
#         elif rand == 1:
#             core.memory("nbCreeps", core.memory("nbCreeps")+1)
#             core.memory("creeps").append(Creep())
#             print("[RANDOM] Creeps are now : ", core.memory("nbCreeps"))

def setup():
    print("Setup START---------")

    core.fps = 30
    core.WINDOW_SIZE = [800, 800]
    
    core.memory("agents", [])
    core.memory("nbSains", 50)
    core.memory("nbInfectes", 5)
    core.memory("nbAgents", core.memory("nbSains")+core.memory("nbInfectes"))

    for i in range(0, core.memory("nbSains")):
        core.memory("agents").append(Agent(Statut.Sains))

    for i in range(0, core.memory("nbInfectes")):
        core.memory("agents").append(Agent(Statut.Infecte))

    print("Setup END-----------")

def reset():
    # empty all lists
    core.memory("sains", [])
    core.memory("infectes", [])

    # init objects 
    for i in range(0, core.memory("nbSains")):
        core.memory("agents").append(Agent(Statut.Sains))

    for i in range(0, core.memory("nbInfectes")):
        core.memory("agents").append(Agent(Statut.Infecte))


def run():
    core.cleanScreen()

    # CONTROL
    if core.getKeyPressList("q"):
        pygame.quit()
    if core.getKeyPressList("r"):
        reset()
        print("[USER Command] Reset environment")
    # if core.getKeyPressList("p"):
    #     core.memory("nbAgents", core.memory("nbAgents")+1)
    #     core.memory("agents").append(Agent())
    #     print("[USER Command] New agent added !")

    # AFFICHAGE
    show()

    # MISE A JOUR DES POSITIONS
    for agent in core.memory("agents"):
        agent.update(core.memory("agents"))
        agent.edge(core.WINDOW_SIZE)

    for agent in core.memory("agents"):
        agent.infect(core.memory("agents"))

    # ADDING RANDOM INFECTED AGENTS TO THE ENVIRONMENT
    # randAddInfectes()

core.main(setup, run)
