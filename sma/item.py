import enum


class Statut(enum.Enum):
   Sains = 1
   Infecte = 2
   Gueri = 3