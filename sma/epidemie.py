epidemie = {
  "duree_incubation": 100,
  "duree_avant_contagions": 10,
  "pourcentage_contagion": 10,
  "duree_avant_deces" : 20,
  "pourcentage_de_mortalite": 5,
  "distance_mini_contagion": 2
}
